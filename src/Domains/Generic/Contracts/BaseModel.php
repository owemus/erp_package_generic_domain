<?php 
namespace ErpPos\Domains\Generic\Contracts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Iatstuti\Database\Support\CascadeSoftDeletes;

class BaseModel extends Model
{
    use RelationshipsTrait;
	use SoftDeletes, CascadeSoftDeletes;

	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

	protected $additionalHidden = [];
}