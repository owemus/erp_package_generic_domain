<?php

namespace ErpPos\Domains\Generic\Contracts;

interface IBaseService
{
	public function Get($page, $itemsperpage, $queryObject);
	public function GetAll($queryObject);
    public function GetWithRelationships($page, $itemsperpage, $queryObject);
    public function Find($id);
    public function FindWithRelationships($id);
    public function Insert($data);
    public function Update($id, $data);
    public function Delete($id);
}