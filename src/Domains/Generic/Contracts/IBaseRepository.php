<?php

namespace ErpPos\Domains\Generic\Contracts;

interface IBaseRepository
{
    public function Get($queryObject);
    public function GetWithRelationships($queryObject);
    public function GetWithPagination($page, $itemsperpage, $queryObject);
    public function GetWithRelationshipsAndPagination($page, $itemsperpage, $queryObject);
    public function GetCount();
    public function Find($id);
    public function FindWithRelationships($id);
    public function Insert($data);
    public function Update($id, $data);
    public function Delete($id);
    public function getValidationRules();
    public function FindAndLock($id);
    public function UpdateAndUnlock($lock);
}