<?php

namespace ErpPos\Domains\Generic\Repositories;

use ErpPos\Domains\Generic\Contracts\IBaseRepository as IBaseRepository;

abstract class BaseRepository implements IBaseRepository
{
    protected $_model;

    function __construct()
    {
    }

    public function Get($queryObject = [])
    {
		$model = $this->_model;
		$query = $this->RetrieveFilter($model, $queryObject);

        return $query->get();
    }

    public function GetWithRelationships($queryObject = [])
    {
        $model = $this->_model;
		$returnRelationships = $this->GetRelationshipsWithRecurrsion($model);
		$query = $this->RetrieveFilter($model, $queryObject, $returnRelationships);

        return $query->with($returnRelationships)->get();
    }

    public function GetWithPagination($page, $itemsperpage, $queryObject = [])
    {
		$model = $this->_model;
		$query = $this->RetrieveFilter($model, $queryObject);

        return $query->orderBy("id", "desc")->skip(($page - 1) * $itemsperpage)->take($itemsperpage)->get();
    }

    public function GetWithRelationshipsAndPagination($page, $itemsperpage, $queryObject = [])
    {
		$model = $this->_model;
		$returnRelationships = $this->GetRelationshipsWithRecurrsion($model);
		$query = $this->RetrieveFilter($model, $queryObject, $returnRelationships);

        return $query->with($returnRelationships)->orderBy("id", "desc")->skip(($page - 1) * $itemsperpage)->take($itemsperpage)->get();
	}
	
	private function RetrieveFilter($model, $queryObject, $relationships = [])
	{
		$query = $model::query();
		
		foreach($queryObject as $key => $value) {
			if (in_array($key, $relationships)) {
				foreach($value as $relGroupFilter) {
					$query->whereHas($key, function($innerQuery) use ($relGroupFilter) {
						foreach($relGroupFilter as $relFilter) {
							$innerQuery->where($relFilter["key"], $relFilter["operator"], $relFilter["value"]);
						}
					});
				}
			}
			else if(is_array($value))
			{
				$query->whereIn($key, $value);
			}
			else 
			{
				$query->where($key, $value);
			}
		}

		return $query;
	}

    public function GetCount()
    {
        $model = $this->_model;
        return $model::count();
    }

    public function Find($id)
    {
        $model = $this->_model;
        return $model::find($id);
    }

    public function FindWithRelationships($id)
    {
        $model = $this->_model;
		$returnRelationships = $this->GetRelationshipsWithRecurrsion($model);

        return $model::with($returnRelationships)->find($id);
	}
	
	private function GetRelationshipsWithRecurrsion($model, $previousRelationship = "") {
		$relationships = (new $model)->relationships();
        $returnRelationships = array();
        
        foreach ($relationships as $relationship => $info)
        {
			if($previousRelationship != "") {
				array_push($returnRelationships, $previousRelationship . $relationship);
			}
			else {
				array_push($returnRelationships, $relationship);
			}

			$returnRelationshipsRecurse = $this->GetRelationshipsWithRecurrsion($info["model"], $previousRelationship . $relationship . ".");
			$returnRelationships = array_merge($returnRelationships,$returnRelationshipsRecurse);
		}

		return $returnRelationships;
	}

    public function Insert($data)
    {
		$model = $this->_model;
		$created = $this->InsertWithRecurrsion($this->_model, $data);

        return $this->FindWithRelationships($created->id);
	}
	
	private function InsertWithRecurrsion($model, $data)
	{
        $relationships = (new $model)->relationships();
        
        $create = $model::create($data);
        
        foreach ($relationships as $relationship => $info)
        {
            if(array_key_exists($relationship, $data))
            {
				foreach($data[$relationship] as $dataRelationship)
				{
					$dataRelationship[$info["foreignKey"]] = $create->id;
					$this->InsertWithRecurrsion($info["model"], $dataRelationship);
				}
            }
		}
		
		return $create;
	}

    public function Update($id, $data)
    {
        $model = $this->_model;
		$data["id"] = $id;
		
		$this->UpdateWithRecurrsion($model, $data);

        return $this->FindWithRelationships($id);
	}

	private function UpdateWithRecurrsion($model, $data) {
		$relationships = (new $model)->relationships();
		$returnRelationships = array();
		
		$id = 0;

		if(isset($data["id"]))
		{
			$update = $model::find($data["id"])->update($data);
			$id = $data["id"];
		}
		else
		{
			$update = $model::create($data);
			$id = $update->id;
		}

        foreach ($relationships as $relationship => $info)
        {
            if(array_key_exists($relationship, $data))
            {
                foreach($data[$relationship] as $dataRelationship)
                {
                    $dataRelationship[$info["foreignKey"]] = $id;
					$this->UpdateWithRecurrsion($info["model"], $dataRelationship);
                }
            }
		}
	}
    
    public function Delete($id)
    {
        $model = $this->_model;
        return $model::destroy($id);
	}

    public function getValidationRules()
    {
        $model = $this->_model;
        return $model::$rules;
    }

    public function FindAndLock($id)
    {
        $model = $this->_model;

        $retrieveAndLock = $model::lockForUpdate()->find($id);

        return $retrieveAndLock;
    }

    public function UpdateAndUnlock($lock)
    {
        $lock->save();

        return true;
    }
}