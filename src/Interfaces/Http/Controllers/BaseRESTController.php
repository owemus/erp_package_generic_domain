<?php 

namespace ErpPos\Interfaces\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request as Request;

class BaseRESTController extends BaseController
{
    protected $_service;

    function __construct()
    {
    }

    public function Get()
    {
        $page = $this->RetrieveGetParam("int", "page", 0);
		$pageLimit = $this->RetrieveGetParam("int", "limit", 10);
		$query = $this->RetrieveGetParam("json", "queryComplex", "[]");
		$queryObject = json_decode(urldecode($query), true);

		$page++;
        
        return $this->_service->Get($page, $pageLimit, $queryObject);
	}

    public function GetWithRelationships()
    {
        $page = $this->RetrieveGetParam("int", "page", 0);
		$pageLimit = $this->RetrieveGetParam("int", "limit", 10);
		$query = $this->RetrieveGetParam("json", "queryComplex", "[]");
		$queryObject = json_decode(urldecode($query), true);
		
		$page++;

        return $this->_service->GetWithRelationships($page, $pageLimit, $queryObject);
	}
	
	public function GetAll()
    {
		$query = $this->RetrieveGetParam("json", "queryComplex", "[]");
		$queryObject = json_decode(urldecode($query), true);

        return $this->_service->GetAll($queryObject);
	}
	
	public function GetAllWithRelationships()
	{
		$query = $this->RetrieveGetParam("json", "queryComplex", "[]");
		$queryObject = json_decode(urldecode($query), true);

        return $this->_service->GetAllWithRelationships($queryObject);
	}

    public function Find($id)
    {
        return $this->_service->Find($id);
    }

    public function FindWithRelationships($id)
    {
        return $this->_service->FindWithRelationships($id);
    }

    public function Insert(Request $request)
    {
        return $this->_service->Insert($request->all());
    }

    public function Update(Request $request, $id)
    {
        return $this->_service->Update($id, $request->all());
    }

    public function Delete($id)
    {
        return $this->_service->Delete($id);
    }

    protected function RetrieveGetParam($type, $name, $default)
    {
        switch($type)
        {
            case "int":
                return (isset($_GET[$name])) ? (int) str_replace(' ', '', $_GET[$name]) : $default;
            case "string":
                return (isset($_GET[$name])) ? str_replace(' ', '', $_GET[$name]) : $default;
            default:
                return (isset($_GET[$name])) ? $_GET[$name] : $default;
        }
    }
}
